= TODO

* [ ] Faire une arborescence
* [ ] Faire une carte d'extérieur et d'intérieur de 5*5*5 hexagone
* [ ] Faire une fonction pour aller taper dans une fausse API plutôt que de consommer des data de NoCodeAPI
* [ ] Gérer les pages précédente dans le locale storage (genre vous êtes pas connecté, connecter vous, puis redirection et suppression du local storage)

* [ ] Faire une diff locale : génération du html en local // prod/dev
* [ ] Faire une commande pour créer les fichier pour un nouveau projet ainsi que le sass