"use strict";

import { createElt } from './tool.js';

export async function getAll(tab) {
    try {
        const response = await fetch(`https://v1.nocodeapi.com/guepe/google_sheets/peIaSPHXbfsVfwWy?tabId=${tab}`, {
            method: "get",
            headers: {
                "Content-Type": "application/json"
            }
        });
        const json = await response.json();
        console.log("Success:", json);
        return json
    } catch (error) {
        console.error("Error:", error);
    }
}

let date = new Date();
let day = date.toLocaleDateString();
let hour = date.toLocaleTimeString();

export async function logToAPI(APIKey, name) {
    try {
        const response = await fetch(`https://v1.nocodeapi.com/guepe/google_sheets/peIaSPHXbfsVfwWy?tabId=log&api_key=${APIKey}`, {
            method: "post",
	        body: JSON.stringify([[`${day} ${hour}`, name]]),
            headers: {
                "Content-Type": "application/json",
            }
        });
        const json = await response.json();
        console.log(json);
    } catch (error) {
        console.error("Error:", error);
    }
}

export async function add(APIKey, tab, arrayValue) {
    try {
        const response = await fetch(`https://v1.nocodeapi.com/guepe/google_sheets/peIaSPHXbfsVfwWy?tabId=${tab}&api_key=${APIKey}`, {
            method: "post",
	        body: JSON.stringify([arrayValue]),
            headers: {
                "Content-Type": "application/json",
            }
        });
        const json = await response.json();
        console.log(json);
    } catch (error) {
        console.error("Error:", error);
    }
}

export function getApiKey() {
    return localStorage.getItem("piogre_api_key");
}

export function checkIfConnected() {
    let name = localStorage.getItem("piogre_player_name");
    let API = localStorage.getItem("piogre_api_key");
    let div = createElt("div", null, null, "connexion");

    if (API !== null && API.length > 0 && name !== null && name.length > 0) {
        div.appendChild(createElt("i", null, null, "fas fa-unlock-alt"));
        div.appendChild(createElt("p", name));
        document.body.appendChild(div);
    } else {
        let a = createElt("a");
        a.href = "./login.html";
        a.title = "login";
        div.appendChild(createElt("i", null, null, "fas fa-lock"));
        a.appendChild(div);
        document.body.appendChild(a);
    }
}