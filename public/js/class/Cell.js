"use strict";

export class Cell {
  constructor(x, y, depth = 0, distance = null, biome = "plain", content = [], obstacle = false) {
    this.x = x;
    this.y = y;
    this.depth = depth;
    this.distance = distance;
    this.biome = biome;
    this.content = content;
    this.obstacle = obstacle;
  }
}
