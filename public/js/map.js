"use strict";

import { Cell } from "./class/Cell.js";

// Map must be odd
// Diamond-square
// Create river

// SRC :
// https://fr.wikipedia.org/wiki/Algorithme_Diamant-Carr%C3%A9
// https://hiko-seijuro.developpez.com/articles/diamond-square/

let heat = false;
let isFat = true;

let heatBtn = document.getElementById("heat-map");
heatBtn.addEventListener("click", () => {
    heat = !heat;
    squareMapDisplay(diamondSquareMap, heat, isFat);
})

let fatBtn = document.getElementById("river");
fatBtn.addEventListener("click", () => {
    isFat = !isFat;
    squareMapDisplay(diamondSquareMap, heat, isFat);
})

let runBtn = document.getElementById("run");
runBtn.addEventListener("click", () => {
    runMap(heat, isFat);
})

let firstPositionMin = new Cell(0, 0);
let firstPositionMax = new Cell(0, 0);

let mapDiv = document.getElementById("map");
let mapIsoDiv = document.getElementById("map-iso");
let mapIsoDiv_2 = document.getElementById("map-iso-2");

function createMap(a, b = a) {
    a = Math.pow(2, a) + 1;
    b = Math.pow(2, b) + 1;

    let array = [];
    for (let i = 0; i < a; i++) {
        array.push([]);
        for (let y = 0; y < b; y++) {
            array[i].push(0);
        }
    }
    return array;
}

function consoleMap(array) {
    array.forEach(row => {
        let rowString = "";
        row.forEach(cell => {
            rowString = rowString.concat(`${cell}`)
        });
        console.log(rowString);
    });
}

function transformWithDiamondSquare(array) {
    let h = array.length;
    let depthMin = 10000;
    let depthMax = 0;
    // Corner initialization
    array[0][0] = rand(-h, h);
    array[0][h-1] = rand(-h, h);
    array[h-1][h-1] = rand(-h, h);
    array[h-1][0] = rand(-h, h);

    let i = h - 1

    while (i > 1) {
        let id = i / 2;
        // Start of the diamond phase
        for (let x = id; x < h; x += i) {
            for (let y = id; y < h; y += i) {
                let average = (array[x - id][y - id] + array[x - id][y + id] + array[x + id][y + id] + array[x + id][y - id]) / 4;
                let value = Math.round(average + rand(-id, id));
                if (value < depthMin) {
                    depthMin = value;
                    firstPositionMin.x = x;
                    firstPositionMin.y = y;
                    firstPositionMin.depth = value;
                }
                if (value > depthMax) {
                    depthMax = value;
                    firstPositionMax.x = x;
                    firstPositionMax.y = y;
                    firstPositionMax.depth = value;
                }
                array[x][y] = value;
            }
        }
        let offset = 0;
        // Start of the square phase
        for (let x = 0; x < h; x += id) {
            offset = (offset === 0) ? id : 0;
            for (let y = offset; y < h; y += i) {
                let sum = 0;
                let n = 0;

                if (x >= id) {
                    sum = sum + array[x - id][y];
                    n = n + 1;
                }
                if (x + id < h) {
                    sum = sum + array[x + id][y];
                    n = n + 1;
                }
                if (y >= id) {
                    sum = sum + array[x][y - id];
                    n = n + 1;
                }
                if (y +id < h) {
                    sum = sum + array[x][y + id];
                    n = n + 1;
                }

                let value = Math.round(sum / n + rand(-id, id))
                if (value < depthMin) {
                    depthMin = value;
                    firstPositionMin.x = x;
                    firstPositionMin.y = y;
                    firstPositionMin.depth = value;
                }
                if (value > depthMax) {
                    depthMax = value;
                    firstPositionMax.x = x;
                    firstPositionMax.y = y;
                    firstPositionMax.depth = value;
                }
                array[x][y] = value;
            }
        }
        i = id;
    }
    return array;
}

function squareMapDisplay(array, heatDisplay = false, fatRiver = true, displayMin = false, displayMax = false) {
    mapDiv.innerHTML = "";
    for (let x = 0; x < array.length; x++) {
        let rowDiv = document.createElement("div");
        rowDiv.classList.add("row");
        for (let y = 0; y < array[0].length; y++) {
            let cellDiv = document.createElement("div");
            cellDiv.classList.add("cell");
            cellDiv.id = `${x}:${y}`;
            cellDiv.dataset.depth = array[x][y];
            if (heatDisplay) {
                cellDiv.style.backgroundColor = `hsl(${array[x][y]}, 50%, 50%)`;
            } else {
                if (array[x][y] < valueMin + step) {
                    cellDiv.style.backgroundColor = `hsl(0, 60%, 50%)`;
                } else if (array[x][y] < valueMin + step * 2) {
                    cellDiv.style.backgroundColor = `hsl(0, 60%, 50%)`;
                } else if (array[x][y] < valueMin + step * 3) {
                    cellDiv.style.backgroundColor = `hsl(0, 60%, 50%)`;
                } else if (array[x][y] < valueMin + step * 4) {
                    cellDiv.style.backgroundColor = `hsl(20, 80%, 50%)`;
                } else if (array[x][y] < valueMin + step * 4.5) {
                    cellDiv.style.backgroundColor = `hsl(40, 90%, 50%)`;
                } else {
                    cellDiv.style.backgroundColor = `hsl(60, 100%, 50%)`;
                }
            }
            if (displayMin && x === firstPositionMin.x && y === firstPositionMin.y) {
                cellDiv.style.backgroundColor = `hsl(300, 100%, 60%)`;
            } else if (displayMax && x === firstPositionMax.x && y === firstPositionMax.y) {
                cellDiv.style.backgroundColor = `hsl(150, 100%, 60%)`;
            }
            rowDiv.appendChild(cellDiv);
        }
        mapDiv.appendChild(rowDiv);
    }
    if (firstPositionMax.x > 3 && firstPositionMax.y > 3 && firstPositionMax.x < array.length - 3 && firstPositionMax.y < array.length - 3) {
        drawRiverFromPosition(firstPositionMax, fatRiver);
    } else {
        let cell = document.getElementById(`${firstPositionMax.x}:${firstPositionMax.y}`);
        cell.style.backgroundColor = `hsl(150, 90%, 50%)`;
    }

}

// Preset intéressant :

// step     : 20%
// step * 2 : 40%
// step * 3 : 60%
// step * 4 : 80%
// else     : 100%

// step         : 60%
// step * 2     : 60%
// step * 3     : 60%
// step * 4     : 80%
// step * 4.5   : 90%
// else         : 100%

function hexMapDisplay(array) {
    mapIsoDiv.innerHTML = "";
    for (let i = 0; i < array.length; i++) {
        let rowDiv = document.createElement("div");
        rowDiv.classList.add("hex-row");
        rowDiv.style.top = `${-i * 14}px`;
        rowDiv.style.right = `${-i * 4}px`;
        array[i].forEach(cell => {
            let img = document.createElement("img");
            img.classList.add("hex-img");
            if (cell < valueMin + step * 3) {
                img.src = "../img/hexa_1.png";
            } else if (cell < valueMin + step * 4) {
                img.src = "../img/hexa_2.png";
            } else if (cell < valueMin + step * 4.5) {
                img.src = "../img/hexa_3.png";
            } else {
                img.src = "../img/hexa_4.png";
            }
            rowDiv.appendChild(img);
        });
        mapIsoDiv.appendChild(rowDiv);
    }
}

function hexMapDisplay_2(array) {
    mapIsoDiv_2.innerHTML = "";
    let odd = true;
    for (let i = 0; i < array.length; i++) {
        let rowDiv = document.createElement("div");
        rowDiv.classList.add("hex-row");
        if (!odd) {
            rowDiv.style.right = "4px";
        }
        odd = !odd;
        rowDiv.style.top = `${-i * 14}px`;
        array[i].forEach(cell => {
            let img = document.createElement("img");
            img.classList.add("hex-img");
            if (cell < valueMin + step * 3) {
                img.src = "../img/hexa_1.png";
            } else if (cell < valueMin + step * 4) {
                img.src = "../img/hexa_2.png";
            } else if (cell < valueMin + step * 4.5) {
                img.src = "../img/hexa_3.png";
            } else {
                img.src = "../img/hexa_4.png";
            }
            rowDiv.appendChild(img);
        });
        mapIsoDiv_2.appendChild(rowDiv);
    }
}

function hexIsoMapDisplay(params) {
    
}

// getRandomIntInclusive
function rand(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min +1) * 5) + min;
}

function runMap(h, r) {
    minArray = [];
    maxArray = [];

    map = createMap(7);
    diamondSquareMap = transformWithDiamondSquare(map);

    diamondSquareMap.forEach(element => {
        minArray.push(Math.min.apply(null, element));
        maxArray.push(Math.max.apply(null, element));
    });

    valueMin = Math.min.apply(null, minArray);
    valueMax = Math.max.apply(null, maxArray);

    total = valueMax - valueMin;

    step = total / 5;

    squareMapDisplay(diamondSquareMap, h, r);
}

let minArray = [];
let maxArray = [];

let map = createMap(7);
let diamondSquareMap = transformWithDiamondSquare(map);

diamondSquareMap.forEach(element => {
    minArray.push(Math.min.apply(null, element));
    maxArray.push(Math.max.apply(null, element));
});

let valueMin = Math.min.apply(null, minArray);
let valueMax = Math.max.apply(null, maxArray);

let total = valueMax - valueMin;

let step = total / 5;

squareMapDisplay(diamondSquareMap);
// hexMapDisplay(diamondSquareMap);
// hexMapDisplay_2(diamondSquareMap);

// Rivière du point le plus haut au point le plus bas ou un coté
// Ajout de lac
// Ajout de côte
// Algorithme de remplissage pour virer tout cell qui ont un bord droit
// Ajouter de manière random des petites montagnes rocks
// implem le Dijkstra

// Fais une ligne droite du point le plus haut au point le plus bas
function findPath(node1, node2) {
    let pathArray = [];
    pathArray.push(node1);
    let x = node1.x;
    let y = node1.y;
    while (x !== node2.x || y !== node2.y) {
        let xPercent = Math.abs(x - node2.x);
        let yPercent = Math.abs(y - node2.y);

        if (x < node2.x && xPercent >= yPercent) x++;
        else if (x > node2.x && xPercent >= yPercent) x--;

        if (y < node2.y && yPercent >= xPercent) y++
        else if (y > node2.y && yPercent >= xPercent) y--;

        let cell = document.getElementById(`${x}:${y}`);
        cell.style.backgroundColor = `hsl(200, 90%, 50%)`;
        pathArray.push(new Cell(x, y));
    }
    return pathArray;
}

// findPath(firstPositionMax, firstPositionMin);

// draw river
function drawRiverFromPosition(greatestNode, fat = true) {
    let nextRow = [greatestNode];
    for (let i = 0; i < 200; i++) {
        let array = getNeighbors(nextRow[nextRow.length - 1], map.length - 1, true);
        let arrayWithoutPreviousResult = [];
        array.forEach(elt => {
            let res = true;
            nextRow.forEach(elt2 => {
                if (elt.x === elt2.x && elt.y === elt2.y) res = false
            });
            if (res) arrayWithoutPreviousResult.push(elt);
        });
        if (arrayWithoutPreviousResult.length > 0) {
            let smallest = getSmallest(arrayWithoutPreviousResult);
            let cell = document.getElementById(`${smallest.x}:${smallest.y}`);
            cell.style.backgroundColor = `hsl(200, 90%, 50%)`;

            if (fat) {
                if (checkNode(new Cell(smallest.x, smallest.y -1), map.length - 1)) {
                    let cell2 = document.getElementById(`${smallest.x}:${smallest.y - 1}`);
                    cell2.style.backgroundColor = `hsl(200, 90%, 50%)`;
                }
                if (checkNode(new Cell(smallest.x, smallest.y +1), map.length - 1)) {
                    let cell2 = document.getElementById(`${smallest.x}:${smallest.y + 1}`);
                    cell2.style.backgroundColor = `hsl(200, 90%, 50%)`;
                }
                if (checkNode(new Cell(smallest.x + 1, smallest.y), map.length - 1)) {
                    let cell2 = document.getElementById(`${smallest.x + 1}:${smallest.y}`);
                    cell2.style.backgroundColor = `hsl(200, 90%, 50%)`;
                }
                if (checkNode(new Cell(smallest.x - 1, smallest.y -1), map.length - 1)) {
                    let cell2 = document.getElementById(`${smallest.x - 1}:${smallest.y - 1}`);
                    cell2.style.backgroundColor = `hsl(200, 90%, 50%)`;
                }
            }

            if (smallest.x === 0 || smallest.y === 0 || smallest.x === map.length - 1 || smallest.y === map.length - 1) {
                return;
            }
            nextRow.push(smallest);
        }
    }
}

function getNeighbors(node, max, direction8 = false) {
    let array = [];

    let north = new Cell(node.x -1, node.y);
    let south = new Cell(node.x +1, node.y);
    let east = new Cell(node.x, node.y - 1);
    let west = new Cell(node.x, node.y + 1);

    if (checkNode(north, max)) array.push(north);
    if (checkNode(south, max)) array.push(south);
    if (checkNode(east, max)) array.push(east);
    if (checkNode(west, max)) array.push(west);

    if (direction8) {
        let northWest = new Cell(node.x -1, node.y - 1);
        let northEast = new Cell(node.x -1, node.y + 1);
        let southWest = new Cell(node.x +1, node.y - 1);
        let southEast = new Cell(node.x +1, node.y + 1);
    
        if (checkNode(northWest, max)) array.push(northWest);
        if (checkNode(northEast, max)) array.push(northEast);
        if (checkNode(southWest, max)) array.push(southWest);
        if (checkNode(southEast, max)) array.push(southEast);
    }

    return array;
}

// Check if value is in the array
// Faire un sorte de faire ça à partir d'un rectangle avec x et y changent pour le min et le max
function checkNode(node, max) {
    if (node.x >= 0 && node.x <= max && node.y >= 0 && node.y <= max) {
        return true;
    }
    return false;
}

// return smallest value from array
function getSmallest(array) {
    let result = null;
    array.forEach(elt => {
        let value = parseInt(document.getElementById(`${elt.x}:${elt.y}`).dataset.depth);
        if (result === null || value < result.depth) {
            result = new Cell(elt.x, elt.y, value);
        }
    });
    return result;
}

function getDijkstra(array, node1, node2) {
    let cell1 = document.getElementById(`${node1.x}:${node1.y}`);
    cell1.style.backgroundColor = "black";
    let cell2 = document.getElementById(`${node2.x}:${node2.y}`);
    cell2.style.backgroundColor = "black";
    let neighbors = getNeighbors(node1, array.length - 1, true);
}

let hero = new Cell(5, 5, null, 0)
let target = new Cell(53, 69, null, 0)

getDijkstra(map, hero, target)

// https://www.ralentirtravaux.com/lettres/cours/conjugaison/verbes-plus-frequents.php

let array1 = ["être", "avoir", "faire", "dire", "aller", "voir", "savoir", "pouvoir", "falloir", "vouloir", "venir", "prendre", "arriver", "croire", "mettre", "passer", "devoir", "parler", "trouver", "donner", "comprendre", "connaître", "partir", "demander", "tenir", "aimer", "penser", "rester", "manger", "appeler", "sortir", "travailler", "acheter", "écouter", "laisser", "entendre", "rentrer", "commencer", "marcher", "regarder", "rendre", "revenir", "lire", "monter", "payer", "chercher", "jouer", "paraître", "attendre", "perdre"];
let array2 = ["être", "avoir", "faire", "dire", "pouvoir", "aller", "voir", "savoir", "vouloir", "venir", "falloir", "devoir", "croire", "trouver", "donner", "prendre", "parler", "aimer", "passer", "mettre", "demander", "tenir", "sembler", "laisser", "rester", "penser", "entendre", "regarder", "répondre", "rendre", "connaître", "paraître", "arriver", "sentir", "attendre", "vivre", "chercher", "sortir", "comprendre", "porter", "entrer", "devenir", "revenir", "écrire", "appeler", "tomber", "reprendre", "commencer", "suivre", "montrer"]

function compareArrayOfString(arr1, arr2) {
    let result = [];
    arr1.forEach(word1 => {
        result.push(word1)
        let word = "";
        let isPresent = false;
        arr2.forEach(word2 => {
            if (word1 === word2) {
                isPresent = true;
                word = word2;
            }
        });
        if (!isPresent && word.length > 0) {
            result.push(word);
        } else {
            if (word.length > 0) console.log(`${word1} = ${word}`);
        }
    });
    console.log(arr1.length);
    console.log(arr2.length);
    console.log(result.length);
    console.log(result);
    return result;
}

compareArrayOfString(array1, array2);
