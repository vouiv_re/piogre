"use strict";

import { elt } from './tool.js';

let main = document.getElementById("main");
let sectionGrid = document.getElementById("section-grid");
let sectionPoint = document.getElementById("section-point");

let points = [];

// Créer la grille
let gridSize = 20;
let cellSize = 40;

/**
 * Set size
 * @param element HTML element
 * @param value integer
 * @param w integer
 * @param h integer
 * @return void
 */
function setSize(element, value, w, h = w) {
    element.style.width = `${w * value}px`;
    element.style.height = `${h * value}px`;
}

/**
 * Get points length
 * @return integer
 */
function getPointsLength() {
    return points.length;
}

/**
 * Remove element from array
 * @param x integer
 * @param y integer
 * @return array
 */
 function removeEltFromArray(x, y) {
    let result = [];
    for (let i = 0; i < points.length; i++) {
        // console.log(points[i]);
        // console.log(points[i].y);
        // console.log(x);
        // console.log(y);
        if (points[i].x === x && points[i].y === y) {
            // console.log("here");
            result = points.splice(i, 1);
            break;
        }
    }
    console.log(points);
    points = result;
    console.log(points);
}

setSize(main, gridSize, cellSize);
setSize(sectionGrid, gridSize, cellSize);
setSize(sectionPoint, gridSize, cellSize);

for (let i = 0; i < gridSize + 1; i++) {
    for (let j = 0; j < gridSize + 1; j++) {
        let hLine = elt("div", {"class": "line h-line"});
        hLine.style.top = `${cellSize * i}px`
        hLine.style.left = `0px`
        let vLine = elt("div", {"class": "line v-line"});

        if (i % 5 === 0) {
            hLine.style.borderColor = "#ababab";
            vLine.style.borderColor = "#ababab";
        }

        if (i % 10 === 0) {
            hLine.style.borderColor = "#ababab";
            hLine.style.borderStyle = "dashed";
            vLine.style.borderColor = "#ababab";
            vLine.style.borderStyle = "dashed";
        }

        vLine.style.top = "0px";
        vLine.style.left = `${cellSize * i}px`;
        
        for (let k = 0; k < gridSize + 1; k++) {
            if (k < gridSize / 2 + 1) {
                let point = elt("div", {"class": "point", "id": `${i}/${k}`});
                point.style.top = `${i * cellSize - 10}px`;
                point.style.left = `${k * cellSize - 10}px`;

                point.addEventListener("click", () => {
                    // Si il existe on le supprime
                    if (point.style.backgroundColor === "black") {
                        point.style.backgroundColor = "";

                        // Remove symmetric point
                        console.log(document.getElementById(`${i}/${gridSize - k}`));
                        document.getElementById(`${i}/${gridSize - k}`).firstChild.remove();
                        removeEltFromArray(i, k);
                    } else {
                        point.style.backgroundColor = "black";

                        let pointNb = elt("p", {"content": getPointsLength() + 1});
                        point.appendChild(pointNb);

                        points.push({"x": i, "y": k});

                        // Si le point n'est pas sur la ligne centrale, sinon il y aurait duplication
                        if (k !== gridSize / 2) {
                            let symmetricPoint = elt("div", {"class": "point", "id": `${i}/${gridSize - k}`});
                            symmetricPoint.style.top = `${i * cellSize - 10}px`;
                            symmetricPoint.style.left = `${gridSize * cellSize - k * cellSize - 10}px`;
                            symmetricPoint.style.backgroundColor = "black";
    
                            sectionPoint.appendChild(symmetricPoint);
                        }
                    }
                })
                
                sectionPoint.appendChild(point);
            }
        }

        sectionGrid.appendChild(hLine);
        if (i < gridSize / 2 + 1) sectionGrid.appendChild(vLine);
    }
}