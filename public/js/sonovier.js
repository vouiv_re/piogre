"use strict";

// * [ ] Problem the setTimeout does not reset. Just visual problem.

import { elt, displayPopup } from './tool.js';

// Get HTML elements
let loader = document.getElementById("loader");
let info = document.getElementById("info");

let btnAdd = document.getElementById("btn-add");
let btnExport = document.getElementById("btn-export");
let btnImport = document.getElementById("uploadInput");
let btnInfoOpen = document.getElementById("btn-info-open");
let btnInfoClose = document.getElementById("btn-info-close");
let btnMenu = document.getElementById("btn-menu");

let sectionMenu = document.getElementById("section-menu");
let sectionBtns = document.getElementById("section-btns");

let title = document.getElementById("title");

let iptURL = document.getElementById("ipt-url");
let iptName = document.getElementById("ipt-name");
let iptIcon = document.getElementById("ipt-icon");

// Get and format date for filename
let today = new Date();
let date = today.getFullYear()+'_'+(today.getMonth()+1)+'_'+today.getDate();

// Detect if its mobile or desktop browser: https://stackoverflow.com/questions/11381673/detecting-a-mobile-browser
let mobileAndTabletCheck = function() {
    let check = false;
    (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
    return check;
};

// Get data from localStorage
let data = JSON.parse(localStorage.getItem("sonovier"));

if (data === null) {
    data = [{"URL":"0194","name":"Argent","icon":"coins"},{"URL":"2112","name":"Monstre","icon":"ghost"},{"URL":"1387","name":"Gouttes d'eau ","icon":"tint"},{"URL":"2135","name":"Caverne","icon":"mountain"}];
    localStorage.setItem("sonovier", JSON.stringify(data));
}

for (const element of data) {
    addBtnSound(element["URL"], element["name"], element["icon"]);
}
loader.classList.add("hidden");

// Button to display info sheet
btnInfoOpen.addEventListener("click", () => {
    info.classList.remove("hidden");
})

// Button to display info sheet
btnInfoClose.addEventListener("click", () => {
    info.classList.add("hidden");
})

// Button to display menu and h1 and add button
btnMenu.addEventListener("click", () => {
    if (sectionMenu.classList.contains("hidden")) {
        sectionMenu.classList.remove("hidden");
        title.classList.remove("hidden");
        btnAdd.classList.remove("hidden");
        btnMenu.firstChild.className = "fas fa-chevron-up";
    } else {
        sectionMenu.classList.add("hidden");
        title.classList.add("hidden");
        btnAdd.classList.add("hidden");
        btnMenu.firstChild.className = "fas fa-chevron-down";
    }
})

// Button to add new sound
btnAdd.addEventListener("click", () => {
    if (iptName.value !== "" && iptURL.value !== "") {
        data.push({
            "URL": iptURL.value,
            "name": iptName.value,
            "icon": iptIcon.value
        });
        localStorage.setItem("sonovier", JSON.stringify(data));

        addBtnSound(iptURL.value, iptName.value, iptIcon.value);
    
        iptURL.value = "";
        iptName.value = "";
        iptIcon.value = "";
    } else {
        displayPopup("Name or/and URL are missing.")
    }
})

/**
 * Create and add sound button to HTML section
 * @param URL string
 * @param name string
 * @param icon string
 * @returns none
 */
function addBtnSound(URL, name, icon) {
    let btnLabel = elt("p", {"content": name});
    let btnIcon = elt("i", {"class": `fas fa-question fa-2x`});
    let audio = document.createElement("audio");
    audio.src = `https://lasonotheque.org/UPLOAD/mp3/${URL}.mp3`;
    
    if (icon !== "") {
        btnIcon = elt("i", {"class": `fas fa-${icon} fa-2x`});
    }
    
    let btn = elt("div", {"additions": [btnLabel, btnIcon, audio], "class": "btn-sound"});
    
    let pressTimer;

    // Action to start or stop music
    btn.addEventListener(mobileAndTabletCheck() ? "touchend" : "mouseup", () => {
        clearTimeout(pressTimer);
        if (btn.classList.contains("play")) {
            btn.classList.remove("play");
            audio.pause();
            audio.currentTime = 0;
        } else {
            audio.play();
            btn.classList.add("play");
            setTimeout(() => {btn.classList.remove("play")}, audio.duration * 1000);
        }
    })

    // Action to remove button
    btn.addEventListener(mobileAndTabletCheck() ? "touchstart" : "mousedown", () => {
        pressTimer = window.setTimeout(function() {
            data = data.filter( el => el.name !== name || el.URL !== URL);
            localStorage.setItem("sonovier", JSON.stringify(data));
            btn.remove();
        }, 1000);
        return false;
    })
    sectionBtns.appendChild(btn);
}

// Button to export data to JSON
btnExport.addEventListener("click", () => {
    let dataStr = JSON.stringify(data);
    let dataUri = 'data:application/json;charset=utf-8,'+ encodeURIComponent(dataStr);
    
    let exportFileDefaultName = `sonovier_save_${date}.json`;
    
    let linkElement = document.createElement('a');
    linkElement.setAttribute('href', dataUri);
    linkElement.setAttribute('download', exportFileDefaultName);
    linkElement.click();
})

// Button to import data from JSON file
btnImport.addEventListener("change", (evt) => {
    var reader = new FileReader();
    let content;
    reader.onload = (evt) => {
        content = JSON.parse(evt.target.result);
        content.forEach(element => {
            let newData = {
                "URL": element["URL"],
                "name": element["name"],
                "icon": element["icon"]
            };
            let checkIfAlreadyPresent = false;
            for (const row of data) {
                if (row["URL"] === newData["URL"] && row["name"] === newData["name"]) {
                    checkIfAlreadyPresent = true;
                    break;
                }
            }
            if (!checkIfAlreadyPresent) {
                data.push(newData);
                localStorage.setItem("sonovier", JSON.stringify(data));
                addBtnSound(element["URL"], element["name"], element["icon"]);
            }
        });
    };
    reader.readAsText(evt.target.files[0]);
})