"use strict";

import { getAll } from './api.js';
import { createElt } from './tool.js';

let main = document.getElementById("main");

getAll("spellbook")
    .then(response => {
        response["data"].forEach(row => {
            let card = createElt("div", null, "card");
            card.appendChild(createElt("h2", row["name"], "name"));

            if (row["notes"] !== "") card.appendChild(createElt("p", row["notes"]));
            if (row["effect"] !== "") card.appendChild(createElt("p", `Effet : ${row["effect"]}`, null, "effect"));
                        
            let section = createElt("section", null);
            if (row["area"] !== "") section.appendChild(createElt("p", `Cible : ${row["area"]}`));
            if (row["motion"] !== "") section.appendChild(createElt("p", `Déplacement : ${row["motion"]}`));
            if (row["level"] !== "") section.appendChild(createElt("p", row["level"]));
            if (row["testType"] !== "") section.appendChild(createElt("p", row["testType"]));
            if (row["applicationType"] !== "") section.appendChild(createElt("p", row["applicationType"]));
            if (row["damage"] !== "") section.appendChild(createElt("p", `Dégâts : ${row["damage"]}`));
            if (row["distance"] !== "") section.appendChild(createElt("p", `Distance : ${row["distance"]}`));
            if (row["duration"] !== "") section.appendChild(createElt("p", `Durée : ${row["duration"]}`));
            if (row["testDifficulty"] !== "") section.appendChild(createElt("p", `Difficulté : ${row["testDifficulty"]}`));
            if (row["actionCost"] !== "") section.appendChild(createElt("p", `Coût : ${row["actionCost"]}`));
            if (row["magicType"] !== "") section.appendChild(createElt("p", row["magicType"]));

            if (row["check"] === "x") {
                let div = createElt("div", null, null, "check");
                div.appendChild(createElt("i", null, null, "fas fa-check-circle"));
                card.appendChild(div);
            }

            card.appendChild(section);

            if (row["description"] !== "") card.appendChild(createElt("p", row["description"], null, "description"));

            main.appendChild(card);
            console.log(row);
        });
    });