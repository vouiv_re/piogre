"use strict";

// https://www.dcode.fr/mots-commencant-par

let wordProposal = [
    "DAMASSER",
    "DEBOISER",
    "DEBOSSER",
    "DEBRASER",
    "DECAUSER",
    "DECRUSER",
    "DEFRISER",
    "DEGOISER",
    "DEGRISER",
    "DEGUISER",
    "DELASSER",
    "DELISSER",
    "DEPACSER",
    "DEPASSER",
    "DEPAYSER",
    "DEPENSER",
    "DEPHASER",
    "DEPRISER",
    "DESOSSER",
    "DETASSER",
    "DETISSER",
    "DEVERSER",
    "DEVISSER",
    "DIALYSER",
    "DIFFUSER",
    "DISPOSER",
    "DUALISER",
];
let sortedWords = [];

let xMissingLetters = ["C", "O", "N", "G"];
let xPresentLettersMisplaced = ["R", "S", "U", "I"];
let xPresentLetters = [];


// lettersPresentWellPlaced

function findWords(words, missingLetters, presentLettersMisplaced, presentLetters) {
    let response = [];
    words.forEach(word => {
        let admissible = true;
    
        for (const letter of missingLetters) {
            if (word.includes(letter)) {
                admissible = false;
                break;
            }
        }
    
        for (const letter of presentLettersMisplaced) {
            if (!word.includes(letter)) {
                admissible = false;
                break;
            }
        }

        for (const letter in presentLetters) {
            if (word[letter.position] === letter.name) {
                admissible = false;
                break;
            }
        }

        if (word[7] === "S") {
            admissible = false;
        }

        if (word[4] === "R") {
            admissible = false;
        }

        if (word[6] !== "E") {
            admissible = false;
        }

        if (word[1] === "E") {
            admissible = false;
        }
    
        if (admissible) {
            response.push(word);
        }
    });
    return response;
}

sortedWords = findWords(wordProposal, xMissingLetters, xPresentLettersMisplaced, xPresentLetters)

console.log(wordProposal.length);
console.log(sortedWords.length);
console.log(sortedWords);

// https://fr.wikipedia.org/wiki/Fr%C3%A9quence_d%27apparition_des_lettres_en_fran%C3%A7ais

let mostCommonLetters = {
    "a": 7.636,
    "b": 0.901,
    "c": 3.260,
    "d": 3.669,
    "e": 14.715,
    "f": 1.066,
    "g": 0.866,
    "h": 0.737,
    "i": 7.529,
    "j": 0.613,
    "k": 0.074,
    "l": 5.456,
    "m": 2.968,
    "n": 7.095,
    "o": 5.796,
    "p": 2.521,
    "q": 1.362,
    "r": 6.693,
    "s": 7.948,
    "t": 7.244,
    "u": 6.311,
    "v": 1.838,
    "w": 0.049,
    "x": 0.427,
    "y": 0.128,
    "z": 0.326
}

function findWordsWithMostCommonAndDifferentLetters(list) {
    let sort = []
    for (const word of list) {
        
    }
    return sort;
}