"use strict";

import { elt } from '../../../js/tool.js';

let main = document.getElementById("main");

export class Dice {
  constructor(id, colorsInit) {
    this.id = id;

    this.top = colorsInit[0];
    this.down = colorsInit[1];
    this.front = colorsInit[2];
    this.back = colorsInit[3];
    this.left = colorsInit[4];
    this.right = colorsInit[5];

    this.behavior = [
        "v", "v", "v",
        "h",
        "v", "v", "v",
        "h",
        "v", "v", "v",
        "h",
        "v", "v", "v",
        "r",
        "v", "v", "v",
        "h",
        "v", "v", "v",
        "h",
        "v", "v", "v",
        "h",
        "v", "v", "v"
    ];
  }

  createHTMLDice() {
    let section = document.getElementById(`dice_${this.id}`);
    section.innerHTML = "";

    section.appendChild(elt("div", {"class": "dicePattern", "id": `dicePattern_${this.id}`, "additions": [
        elt("div", {"class": `diceSide side_${this.top}`}),
        elt("div", {"class": 'row', "additions": [
            elt("div", {"class": `diceSide side_${this.left}`}),
            elt("div", {"class": `diceSide side_${this.front}`}),
            elt("div", {"class": `diceSide side_${this.right}`})
        ]}),
        elt("div", {"class": `diceSide side_${this.down}`}),
        elt("div", {"class": `diceSide side_${this.back}`})
    ]}))
  }

  generate() {
    this.moveVertically();
    this.moveHorizontally();
    this.moveVertically();
    this.moveHorizontally();
    this.moveVertically();
    this.rotate();
    this.moveVertically();
    this.moveHorizontally();
    this.moveVertically();
    this.moveHorizontally();
    this.moveVertically();
  }

  rotate() {
    this.updateDice(this.right, this.left, null, null, this.top, this.down);
  }

  moveHorizontally() {
    this.updateDice(null, null, this.left, this.right, this.back, this.front);
  }

  moveVertically() {
    for (let i = 0; i < 3; i++) {
        this.updateDice(this.back, this.front, this.top, this.down, null, null);
    }
  }

  updateDice(top, down, front, back, left, right) {
    if (top !== null) this.top = top;
    if (down !== null) this.down = down;
    if (front !== null) this.front = front;
    if (back !== null) this.back = back;
    if (left !== null) this.left = left;
    if (right !== null) this.right = right;

    this.createHTMLDice();
  }

  nextMove() {
    if (this.behavior.length === 0) {
        this.behavior = [
            "v", "v", "v",
            "h",
            "v", "v", "v",
            "h",
            "v", "v", "v",
            "h",
            "v", "v", "v",
            "r",
            "v", "v", "v",
            "h",
            "v", "v", "v",
            "h",
            "v", "v", "v",
            "h",
            "v", "v", "v"
        ];
        return false;
    } else {
        if (this.behavior[0] === "v") {
            this.moveVertically();
        } else if (this.behavior[0] === "h") {
            this.moveHorizontally();
        } else if (this.behavior[0] === "r") {
            this.rotate();
        }
        this.behavior.shift();
        return true;
    }
  }
}
