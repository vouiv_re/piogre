"use strict";
var TestClass = (function () {
    function TestClass(value) {
        this.value = value;
    }
    TestClass.prototype.increment = function () {
        this.value++;
        this.display();
    };
    TestClass.prototype.decrement = function () {
        this.value--;
        this.display();
    };
    TestClass.prototype.display = function () {
        console.log(this.value);
    };
    return TestClass;
}());
export { TestClass };
